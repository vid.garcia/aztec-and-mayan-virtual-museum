/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');

var TouchGestures = require('TouchGestures');

var hexmenu = Scene.root.find('HexMenu');
var aztecs = Scene.root.find('AZTEC');
var mayan = Scene.root.find('MAYAN');

var aztec_sidemenu = Scene.root.find('Sidemenu');
var aztec_back = Scene.root.find('BackButton');
var mictlan_sidebut = Scene.root.find('SidehexMictlan');
var centeotl_sidebut = Scene.root.find('SidehexCenteotl');
var watergodess_sidebut = Scene.root.find('SidehexWaterGodess');

var mayan_sidemenu = Scene.root.find('RuinsSidemenu');
var mayan_back = Scene.root.find('BackFromRuins');

var tulum = Scene.root.find('Tulum Mayan Ruins');
var mictlan = Scene.root.find('MictlanStatue');
var centeotl = Scene.root.find('Aztec statuette');
var watergodess = Scene.root.find('WaterGodessStatue');

TouchGestures.onTap(aztecs).subscribe(function() {
    Diagnostics.log('show aztecs');
        hexmenu.hidden = true;
        aztec_sidemenu.hidden = false;
        watergodess.hidden = false;
});

TouchGestures.onTap(aztec_back).subscribe(function() {
    Diagnostics.log('show mainmenu');
        aztec_sidemenu.hidden = true;
        hexmenu.hidden = false;

        mictlan.hidden = true;
        centeotl.hidden = true;
        watergodess.hidden = true;
});

TouchGestures.onTap(mayan).subscribe(function() {
    Diagnostics.log('show mayan');
        hexmenu.hidden = true;
        tulum.hidden = false;
        mayan_sidemenu.hidden = false;
        //activar una statuette aquí !!
});

TouchGestures.onTap(mayan_back).subscribe(function() {
    Diagnostics.log('show mainmenu');
        mayan_sidemenu.hidden = true;
        tulum.hidden = true;
        hexmenu.hidden = false;
});

TouchGestures.onTap(mictlan_sidebut).subscribe(function() {
    Diagnostics.log('show mictlan');
        mictlan.hidden = false;
        centeotl.hidden = true;
        watergodess.hidden = true;
});

TouchGestures.onTap(centeotl_sidebut).subscribe(function() {
    Diagnostics.log('show mictlan');
        mictlan.hidden = true;
        centeotl.hidden = false;
        watergodess.hidden = true;
});

TouchGestures.onTap(watergodess_sidebut).subscribe(function() {
    Diagnostics.log('show mictlan');
        mictlan.hidden = true;
        centeotl.hidden = true;
        watergodess.hidden = false;
});
