/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');

var TouchGestures = require('TouchGestures');

var watergodessButton = Scene.root.find('WaterGodessButton');
var pyramidofthemoonButton = Scene.root.find('PyramidOfTheMoonButton');
var watergodesscodexButton = Scene.root.find('WaterGodessCodexButton');

var watergodess = Scene.root.find('WaterGodess');
var pyramidofthemoon = Scene.root.find('PyramidOfTheMoon');
var watergodesscodex = Scene.root.find('WaterGodessCodex');

var watergodess_hidden = true;
var pyramidofthemoon_hidden = true;
var watergodesscodex_hidden = true;

//pulses:
TouchGestures.onTap(watergodessButton).subscribe(function() {
    Diagnostics.log('watergodess pulse');
        togglePanel(watergodess);
});

TouchGestures.onTap(pyramidofthemoonButton).subscribe(function() {
    Diagnostics.log('pyramidofthemoon pulse');
    togglePanel(pyramidofthemoon);
});

TouchGestures.onTap(watergodesscodexButton).subscribe(function() {
    Diagnostics.log('watergodesscodex pulse');
    togglePanel(watergodesscodex);
});

//panels:
TouchGestures.onTap(watergodess).subscribe(function() {
    Diagnostics.log('watergodess panel');
    togglePanel(watergodess);
});
TouchGestures.onTap(pyramidofthemoon).subscribe(function() {
    Diagnostics.log('pyramidofthemoon panel');
    togglePanel(pyramidofthemoon);
});

TouchGestures.onTap(watergodesscodex).subscribe(function() {
    Diagnostics.log('watergodesscodex panel');
    togglePanel(watergodesscodex);
});

var togglePanel = function(panel) {
  Diagnostics.log('selected: ' + panel.identifier);

  Diagnostics.log('comparing with: ' + watergodess.identifier);
    if(panel.identifier == watergodess.identifier) {
      Diagnostics.log('match watergodess, toggling...');
      watergodess_hidden = !watergodess_hidden;
      watergodess.hidden = watergodess_hidden;
      Diagnostics.log('closing all others...');
      pyramidofthemoon.hidden = true;
      pyramidofthemoon_hidden = true;
      watergodesscodex.hidden = true;
      watergodesscodex_hidden = true;
      Diagnostics.log('done');
    }
  Diagnostics.log('comparing with: ' + pyramidofthemoon.identifier);
  if(panel.identifier == pyramidofthemoon.identifier) {
    Diagnostics.log('match pyramidofthemoon, toggling...');
    pyramidofthemoon_hidden = !pyramidofthemoon_hidden;
    pyramidofthemoon.hidden = pyramidofthemoon_hidden;
    Diagnostics.log('closing all others...');
    watergodess.hidden = true;
    watergodess_hidden = true;
    watergodesscodex.hidden = true;
    watergodesscodex_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + watergodesscodex.identifier);
  if(panel.identifier == watergodesscodex.identifier) {
    Diagnostics.log('match watergodesscodex, toggling...');
    watergodesscodex_hidden = !watergodesscodex_hidden;
    watergodesscodex.hidden = watergodesscodex_hidden;
    Diagnostics.log('closing all others...');
    pyramidofthemoon.hidden = true;
    pyramidofthemoon_hidden = true;
    watergodess.hidden = true;
    watergodess_hidden = true;
    Diagnostics.log('done');
  }

}
