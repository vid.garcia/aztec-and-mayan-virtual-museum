/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');

var TouchGestures = require('TouchGestures');

var elcastilloButton = Scene.root.find('ElCastilloButton');
var descendinggodButton = Scene.root.find('DescendingGodButton');
var frescoesButton = Scene.root.find('FrescoesButton');
var thewallButton = Scene.root.find('TheWallButton');

var elcastillo = Scene.root.find('ElCastillo');
var descendinggod = Scene.root.find('DescendingGod');
var frescoes = Scene.root.find('Frescoes');
var thewall = Scene.root.find('TheWall');

var elcastillo_hidden = true;
var descendinggod_hidden = true;
var frescoes_hidden = true;
var thewall_hidden = true;

Diagnostics.log('ElCastilloButton id: ' + elcastilloButton.identifier);
Diagnostics.log('frescoesButton id: ' + frescoesButton.identifier);

//pulses:
TouchGestures.onTap(elcastilloButton).subscribe(function() {
    Diagnostics.log('ElCastillo pulse');
        togglePanel(elcastillo);
});

TouchGestures.onTap(descendinggodButton).subscribe(function() {
    Diagnostics.log('DescendingGod pulse');
    togglePanel(descendinggod);
});

TouchGestures.onTap(frescoesButton).subscribe(function() {
    Diagnostics.log('Frescoes pulse');
    togglePanel(frescoes);
});

TouchGestures.onTap(thewallButton).subscribe(function() {
    Diagnostics.log('TheWall pulse');
    togglePanel(thewall);
});

//panels:
TouchGestures.onTap(elcastillo).subscribe(function() {
    Diagnostics.log('ElCastillo panel');
    togglePanel(elcastillo);
});
TouchGestures.onTap(descendinggod).subscribe(function() {
    Diagnostics.log('DescendingGod panel');
    togglePanel(descendinggod);
});

TouchGestures.onTap(frescoes).subscribe(function() {
    Diagnostics.log('Frescoes panel');
    togglePanel(frescoes);
});

TouchGestures.onTap(thewall).subscribe(function() {
    Diagnostics.log('TheWall panel');
    togglePanel(thewall);
});

var togglePanel = function(panel) {
  Diagnostics.log('selected: ' + panel.identifier);

  Diagnostics.log('comparing with: ' + elcastillo.identifier);
    if(panel.identifier == elcastillo.identifier) {
      Diagnostics.log('match ElCastillo, toggling...');
      elcastillo_hidden = !elcastillo_hidden;
      elcastillo.hidden = elcastillo_hidden;
      Diagnostics.log('closing all others...');
      descendinggod.hidden = true;
      descendinggod_hidden = true;
      frescoes.hidden = true;
      frescoes_hidden = true;
      thewall.hidden = true;
      thewall_hidden = true;
      Diagnostics.log('done');
    }
  Diagnostics.log('comparing with: ' + descendinggod.identifier);
  if(panel.identifier == descendinggod.identifier) {
    Diagnostics.log('match DescendingGod, toggling...');
    descendinggod_hidden = !descendinggod_hidden;
    descendinggod.hidden = descendinggod_hidden;
    Diagnostics.log('closing all others...');
    elcastillo.hidden = true;
    elcastillo_hidden = true;
    frescoes.hidden = true;
    frescoes_hidden = true;
    thewall.hidden = true;
    thewall_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + frescoes.identifier);
  if(panel.identifier == frescoes.identifier) {
    Diagnostics.log('match Frescoes, toggling...');
    frescoes_hidden = !frescoes_hidden;
    frescoes.hidden = frescoes_hidden;
    Diagnostics.log('closing all others...');
    descendinggod.hidden = true;
    descendinggod_hidden = true;
    elcastillo.hidden = true;
    elcastillo_hidden = true;
    thewall.hidden = true;
    thewall_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + thewall.identifier);
  if(panel.identifier == thewall.identifier) {
    Diagnostics.log('match TheWall, toggling...');
    thewall_hidden = !thewall_hidden;
    thewall.hidden = thewall_hidden;
    Diagnostics.log('closing all others...');
    descendinggod.hidden = true;
    descendinggod_hidden = true;
    frescoes.hidden = true;
    frescoes_hidden = true;
    elcastillo.hidden = true;
    elcastillo_hidden = true;
    Diagnostics.log('done');
  }

}
