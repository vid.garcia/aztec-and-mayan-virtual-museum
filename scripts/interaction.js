/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');

var TouchGestures = require('TouchGestures');

var maizeButton = Scene.root.find('MaizeButton');
var dualityButton = Scene.root.find('DualityButton');
var sacrificesButton = Scene.root.find('SacrificesButton');
var representationButton = Scene.root.find('RepresentationButton');

var maize = Scene.root.find('Maize');
var duality = Scene.root.find('Duality');
var sacrifices = Scene.root.find('Sacrifices');
var representation = Scene.root.find('Representation');

var maize_hidden = true;
var duality_hidden = true;
var sacrifices_hidden = true;
var representation_hidden = true;

//pulses:
TouchGestures.onTap(maizeButton).subscribe(function() {
    Diagnostics.log('Maize pulse');
        togglePanel(maize);
});

TouchGestures.onTap(dualityButton).subscribe(function() {
    Diagnostics.log('Duality pulse');
    togglePanel(duality);
});

TouchGestures.onTap(sacrificesButton).subscribe(function() {
    Diagnostics.log('Sacrifices pulse');
    togglePanel(sacrifices);
});

TouchGestures.onTap(representationButton).subscribe(function() {
    Diagnostics.log('Representation pulse');
    togglePanel(representation);
});

//panels:
TouchGestures.onTap(maize).subscribe(function() {
    Diagnostics.log('Maize panel');
    togglePanel(maize);
});
TouchGestures.onTap(duality).subscribe(function() {
    Diagnostics.log('Duality panel');
    togglePanel(duality);
});

TouchGestures.onTap(sacrifices).subscribe(function() {
    Diagnostics.log('Sacrifices panel');
    togglePanel(sacrifices);
});

TouchGestures.onTap(representation).subscribe(function() {
    Diagnostics.log('Representation panel');
    togglePanel(representation);
});

var togglePanel = function(panel) {
  Diagnostics.log('selected: ' + panel.identifier);

  Diagnostics.log('comparing with: ' + maize.identifier);
    if(panel.identifier == maize.identifier) {
      Diagnostics.log('match Maize, toggling...');
      maize_hidden = !maize_hidden;
      maize.hidden = maize_hidden;
      Diagnostics.log('closing all others...');
      duality.hidden = true;
      duality_hidden = true;
      sacrifices.hidden = true;
      sacrifices_hidden = true;
      representation.hidden = true;
      representation_hidden = true;
      Diagnostics.log('done');
    }
  Diagnostics.log('comparing with: ' + duality.identifier);
  if(panel.identifier == duality.identifier) {
    Diagnostics.log('match Duality, toggling...');
    duality_hidden = !duality_hidden;
    duality.hidden = duality_hidden;
    Diagnostics.log('closing all others...');
    maize.hidden = true;
    maize_hidden = true;
    sacrifices.hidden = true;
    sacrifices_hidden = true;
    representation.hidden = true;
    representation_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + sacrifices.identifier);
  if(panel.identifier == sacrifices.identifier) {
    Diagnostics.log('match Sacrifices, toggling...');
    sacrifices_hidden = !sacrifices_hidden;
    sacrifices.hidden = sacrifices_hidden;
    Diagnostics.log('closing all others...');
    duality.hidden = true;
    duality_hidden = true;
    maize.hidden = true;
    maize_hidden = true;
    representation.hidden = true;
    representation_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + representation.identifier);
  if(panel.identifier == representation.identifier) {
    Diagnostics.log('match Representation, toggling...');
    representation_hidden = !representation_hidden;
    representation.hidden = representation_hidden;
    Diagnostics.log('closing all others...');
    duality.hidden = true;
    duality_hidden = true;
    sacrifices.hidden = true;
    sacrifices_hidden = true;
    maize.hidden = true;
    maize_hidden = true;
    Diagnostics.log('done');
  }

}
