/**
 * (c) Facebook, Inc. and its affiliates. Confidential and proprietary.
 */

//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Scene = require('Scene');

// Use export keyword to make a symbol available in scripting debug console
export const Diagnostics = require('Diagnostics');

// To use variables and functions across files, use export/import keyword
// export const animationDuration = 10;

// Use import keyword to import a symbol from another file
// import { animationDuration } from './script.js'

// To access scene objects
// const directionalLight = Scene.root.find('directionalLight0');

// To access class properties
// const directionalLightIntensity = directionalLight.intensity;

// To log messages to the console
// Diagnostics.log('Console message logged from the script.');

var TouchGestures = require('TouchGestures');

var mictlanButton = Scene.root.find('MictlanButton');
var underworldButton = Scene.root.find('UnderworldButton');
var codexButton = Scene.root.find('CodexButton');

var mictlan = Scene.root.find('Mictlan');
var underworld = Scene.root.find('Underworld');
var codex = Scene.root.find('Codex');

var mictlan_hidden = true;
var underworld_hidden = true;
var codex_hidden = true;

//pulses:
TouchGestures.onTap(mictlanButton).subscribe(function() {
    Diagnostics.log('mictlan pulse');
        togglePanel(mictlan);
});

TouchGestures.onTap(underworldButton).subscribe(function() {
    Diagnostics.log('underworld pulse');
    togglePanel(underworld);
});

TouchGestures.onTap(codexButton).subscribe(function() {
    Diagnostics.log('codex pulse');
    togglePanel(codex);
});

//panels:
TouchGestures.onTap(mictlan).subscribe(function() {
    Diagnostics.log('mictlan panel');
    togglePanel(mictlan);
});
TouchGestures.onTap(underworld).subscribe(function() {
    Diagnostics.log('underworld panel');
    togglePanel(underworld);
});

TouchGestures.onTap(codex).subscribe(function() {
    Diagnostics.log('codex panel');
    togglePanel(codex);
});

var togglePanel = function(panel) {
  Diagnostics.log('selected: ' + panel.identifier);

  Diagnostics.log('comparing with: ' + mictlan.identifier);
    if(panel.identifier == mictlan.identifier) {
      Diagnostics.log('match mictlan, toggling...');
      mictlan_hidden = !mictlan_hidden;
      mictlan.hidden = mictlan_hidden;
      Diagnostics.log('closing all others...');
      underworld.hidden = true;
      underworld_hidden = true;
      codex.hidden = true;
      codex_hidden = true;
      Diagnostics.log('done');
    }
  Diagnostics.log('comparing with: ' + underworld.identifier);
  if(panel.identifier == underworld.identifier) {
    Diagnostics.log('match underworld, toggling...');
    underworld_hidden = !underworld_hidden;
    underworld.hidden = underworld_hidden;
    Diagnostics.log('closing all others...');
    mictlan.hidden = true;
    mictlan_hidden = true;
    codex.hidden = true;
    codex_hidden = true;
    Diagnostics.log('done');
  }
  Diagnostics.log('comparing with: ' + codex.identifier);
  if(panel.identifier == codex.identifier) {
    Diagnostics.log('match codex, toggling...');
    codex_hidden = !codex_hidden;
    codex.hidden = codex_hidden;
    Diagnostics.log('closing all others...');
    underworld.hidden = true;
    underworld_hidden = true;
    mictlan.hidden = true;
    mictlan_hidden = true;
    Diagnostics.log('done');
  }

}
